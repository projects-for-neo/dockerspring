package ru.neoflex.courses.demo.dockerspring.model;

import lombok.Data;

@Data
public class Person {
    private String name;
}
