package ru.neoflex.courses.demo.dockerspring.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.neoflex.courses.demo.dockerspring.model.Person;

import java.util.Optional;

@RestController
@RequestMapping("/api/v1/")
public class HelloController {

    @GetMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getHello() {
        return new ResponseEntity<String>("Hello from Spring!",
                HttpStatus.OK);
    }

    @PostMapping(value = "/hello-personal",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getPersonalHello(@RequestBody Person person) {
        var name = Optional.ofNullable(person.getName()).orElse("Anonimous");

        if (name.isEmpty() || name.isBlank()) {
            return new ResponseEntity<>("Something went wrong!", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<String>("Hello from Spring, " + name + "!",
                HttpStatus.OK);
    }
}
