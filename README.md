# dockerspring
### Запускаем приложение Spring Boot в Docker
1. В ветке master находится самый базовый образ - 
   нужно собрать c помощью mvn install перед запуском docker build .
2. Ветка continue1. Добавлен файл docker-compose.yml, в котором создается
   и запускается контейнер с postgresql.
3. Ветка continue2. В docker-compose.yml добавлен сервис с текущим приложением,
сборка осуществляется согласно Dockerfile
4. Ветка multistage. Пример двухстадийной сборки контейнера приложения.    
